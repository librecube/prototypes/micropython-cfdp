import logging
import time
import network

import cfdp
from cfdp.transport import UdpTransport
from cfdp.filestore import NativeFileStore


logging.basicConfig(level=logging.DEBUG)


def connect_wlan(essid, password):
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    if not wlan.isconnected():
        print("Connecting to network...", end="")
        wlan.connect(essid, password)
        while not wlan.isconnected():
            pass
        print("done")
    my_ip = wlan.ifconfig()[0]
    return my_ip


def run(essid, password):
    my_ip = connect_wlan(essid, password)

    config = cfdp.Config(
        local_entity=cfdp.LocalEntity(
            1, my_ip + ":5551"),
        remote_entities=[cfdp.RemoteEntity(
            2, "192.168.2.109:5552")],
        filestore=NativeFileStore(""),
        transport=UdpTransport())

    cfdp_entity = cfdp.CfdpEntity(config)

    transaction_id = cfdp_entity.put(
        destination_id=2,
        source_filename="small.txt",
        destination_filename="small.txt",
        transmission_mode=cfdp.TransmissionMode.UNACKNOWLEDGED)

    while not cfdp_entity.is_complete(transaction_id):
        time.sleep(0.1)

    input("Press <Enter> to finish.\n")
    cfdp_entity.shutdown()
