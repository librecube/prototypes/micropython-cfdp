import logging
import network

import cfdp
from cfdp.transport import UdpTransport
from cfdp.filestore import NativeFileStore


logging.basicConfig(level=logging.DEBUG)


def connect_wlan(essid, password):
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    if not wlan.isconnected():
        print("Connecting to network...", end="")
        wlan.connect(essid, password)
        while not wlan.isconnected():
            pass
        print("done")
    my_ip = wlan.ifconfig()[0]
    return my_ip


def run(essid, password):
    my_ip = connect_wlan(essid, password)

    config = cfdp.Config(
        local_entity=cfdp.LocalEntity(
            1, my_ip + ":5551"),
        remote_entities=[cfdp.RemoteEntity(
            2, "192.168.2.109:5552")],
        filestore=NativeFileStore(""),
        transport=UdpTransport())

    cfdp_entity = cfdp.CfdpEntity(config)
    cfdp_entity.transport.bind()

    input("Server is running [{}]. Press <Enter> to stop...\n".format(my_ip))

    cfdp_entity.transport.unbind()
    cfdp_entity.shutdown()
