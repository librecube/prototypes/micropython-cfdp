import logging

import cfdp
from cfdp.transport import UdpTransport
from cfdp.filestore import NativeFileStore


logging.basicConfig(level=logging.DEBUG)


config = cfdp.Config(
    local_entity=cfdp.LocalEntity(
        2, "192.168.2.109:5552"),
    remote_entities=[cfdp.RemoteEntity(
        1, "192.168.2.138:5551")],
    filestore=NativeFileStore("."),
    transport=UdpTransport())

cfdp_entity = cfdp.CfdpEntity(config)
config.transport.bind()

input("Running. Press <Enter> to stop...\n")

config.transport.unbind()
cfdp_entity.shutdown()
