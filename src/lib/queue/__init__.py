
class Empty(Exception):
    pass


class Queue:

    def __init__(self):
        self.buffer = []

    def put(self, item):
        self.buffer.append(item)

    def get(self, block=False, timeout=None):
        if not self.buffer:
            raise Empty()
        return self.buffer.pop(0)
