import os
import random
import binascii

from .base import VirtualFileStore


class NativeFileStore(VirtualFileStore):

    """
    To be written
    
    """

    def __init__(self, rootpath):
        super().__init__()
        self.rootpath = rootpath

    def get_native_path(self, path):
        # convert from virtual filestore path to absolute native path
        return self.rootpath + path

    def get_virtual_path(self, path):
        # convert from native relative path to virtual path
        if self.rootpath in path:
            path = path.replace(self.rootpath, '')
        if not path.startswith('/'):
            path = '/' + path
        return path

    def create_file(self, filepath):
        NativeFileHandle(self.get_native_path(filepath), 'wb').close()

    def delete_file(self, filepath):
        os.remove(self.get_native_path(filepath))

    def rename_file(self, filepath1, filepath2):
        os.rename(
            self.get_native_path(filepath1),
            self.get_native_path(filepath2))

    def append_file(self, filepath1, filepath2):
        fh1 = open(self.get_native_path(filepath1), 'ab')
        fh2 = open(self.get_native_path(filepath2), 'rb')
        fh1.write(fh2.read())
        fh1.close()
        fh2.close()

    def replace_file(self, filepath1, filepath2):
        fh1 = open(self.get_native_path(filepath1), 'wb')
        fh2 = open(self.get_native_path(filepath2), 'rb')
        fh1.write(fh2.read())
        fh1.close()
        fh2.close()

    def create_directory(self, dirpath):
        os.mkdir(self.get_native_path(dirpath))

    def remove_directory(self, dirpath):
        try:
            if os.stat(dirpath)[0] & 0x4000:
                for f in os.ilistdir(dirpath):
                    if f[0] not in ('.', '..'):
                        self.remove_directory(
                            "/".join((dirpath, f[0])))
                os.rmdir(dirpath)
            else:
                os.remove(dirpath)
        except:

            print("remove_directory of '%s' failed" % dirpath)

    def list_directory(self, dirpath):
        # create directory listing
        dirpath = self.get_native_path(dirpath)
        # get the full path w.r.t to host file system
        list_dir = os.ilistdir(dirpath)

        listing = "type,path,size,timestamp\n"

        for i in list_dir:
            if i[1] == 0x4000:
                line = "{},{},{},{}\n".format(
                    "d",
                    i[0],
                    i[3],
                    0)  # Set default timestamp to zero
            elif i[1] == 0x8000:
                line = "{},{},{},{}\n".format(
                    "f",
                    i[0],
                    i[3],
                    0)  # Set default timestamp to zero

            listing += line
        return listing

    def open(self, filepath, mode='rb'):
        return NativeFileHandle(self.get_native_path(filepath), mode)

    def open_tempfile(self):
        return NativeTemporaryFileHandle()

    def is_file(self, filepath):
        try:
            if os.stat(filepath)[0] & 0x8000:
                return True
            else:
                return False
        except:
            return False

    def get_size(self, filepath):
        file_open = open(filepath, 'rb')
        file_size = file_open.seek(0, 2)
        file_open.close()
        return file_size

    def join_path(self, *args):
        return '/'.join(args)


class NativeFileHandle:

    """
    The NativeFileHandle provides a generic interface to Python's 'open'
    function. It also implements method for calculating the file checksum.

    """

    def __init__(self, filepath, mode='rb'):
        self.handle = open(filepath, mode)

    def get_file_size(self):
        pass

    def seek(self, *args, **kwargs):
        return self.handle.seek(*args, **kwargs)

    def tell(self, *args, **kwargs):
        return self.handle.tell(*args, **kwargs)

    def read(self, *args, **kwargs):
        return self.handle.read(*args, **kwargs)

    def write(self, *args, **kwargs):
        return self.handle.write(*args, **kwargs)

    def close(self, *args, **kwargs):
        return self.handle.close(*args, **kwargs)

    def calculate_checksum(self, type):
        if type == 0:  # Modular, see Annex F of CCSC 727.0-B-5
            file_size = self.handle.seek(0, 2)
            checksum = 0
            a = b''
            x = 0
            while x < file_size:
                self.handle.seek(x)
                if x > file_size - 4:
                    a = self.handle.read(file_size % 4)
                    int_a = int(binascii.hexlify(a), 16)\
                        * 2**((4 - file_size % 4) * 8)
                else:
                    a = self.handle.read(4)
                    int_a = int(binascii.hexlify(a), 16)
                checksum = checksum + int_a
                x = x + 4
            checksum = checksum % 2**32
            return checksum
        elif type == 15:  # Null checksum
            return 0
        else:
            # TODO: implemented other checksum algorithms
            raise NotImplementedError


class NativeTemporaryFileHandle(NativeFileHandle):

    def __init__(self):
        self.filename = str(random.random()) + '.tmp'
        self.handle = open(self.filename, 'wb')
