import _thread

from cfdp import logger
from cfdp.event import Event


class Timer:

    def __init__(self, machine, timeout, raise_event, expiration_limit=None):
        self.machine = machine
        self.timeout = timeout
        self.raise_event = raise_event
        self._timer = None
        self._expiration_limit = expiration_limit
        self._expiration_counter = 0
        self.suspended = False
        self._lock = _thread.allocate_lock()

    def restart(self):
        if self._lock.locked():
            self._lock.release()

        if self.timeout:
            self._lock.acquire(1, self.timeout)
            if not self._lock.locked():
                self._timer = _thread.start_new_thread(self._expired, ())

    def cancel(self):
        if self._lock.locked():
            self._lock.release()

    def _expired(self):
        logger.debug("Timer expired {}".format(type(self)))
        self._expiration_counter += 1
        self.machine.kernel.trigger_event(
            Event(self.machine.transaction, self.raise_event))

    def is_limit_reached(self):
        if self._expiration_limit is None:
            return False
        else:
            return self._expiration_counter >= self._expiration_limit

    def shutdown(self):
        self.cancel()


class InactivityTimer(Timer):
    pass


class AckTimer(Timer):
    pass


class NakTimer(Timer):
    pass
