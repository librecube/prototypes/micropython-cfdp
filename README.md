# MicroPython CFDP

This is a port of the [Python CFDP implementation](https://gitlab.com/librecube/prototypes/python-cfdp) to MicroPython. Currently only the
ESP32 board is supported.

## Installation

To add the `cfdp` module to the ESP32 board that runs MicroPython, one needs to
copy to the board's root directory the folders `cfdp` and `lib`.

## Development Setup

The programming of ESP32 is done easily using [Thonny IDE](https://thonny.org/).

Also, the ESP32 must be running [MicroPython](https://micropython.org/download/esp32/) the instructions. To do so:

- Install esptool via pip: `pip install esptool`.
- Erase the ESP32: `esptool.py --chip esp32 --port /dev/ttyUSB0 erase_flash`
- Upload latest firmware: `esptool.py --chip esp32 --port /dev/ttyUSB0 --baud 460800 write_flash -z 0x1000 bin/esp32-20210418-v1.15.bin`

Then use Thonny IDE to connect to the board. You should see that only a file
`boot.py` exists on the board.

Now copy over the folders `cfdp` and `lib` from `build` to the ESP32 root directory.

> Note that the folder `build` contains the compiled version of the files in the
`src` folder. In order to re-compile (eg. after changes in src), run
`python compile.py src -o build`. For this you need to have mpy-cross installed:
`pip install mpy-cross`.

Next, copy the file `examples/esp32/server.py` to root directoy of ESP32.

Finally, run `import server; server.run("your wlan essid", "your password")`.
Alternatively, add it to `boot.py` to start the server upon reboot of ESP32.

Now a CFDP entity is running on your ESP32 and you can connect
to it with other CFDP entities. See `examples/esp32/server.py` for configuration
details.

To start a CFDP client locally on your computer:

- Install Python-CFDP: `pip install cfdp`
- Run `python example/local/client.py` or write your own client.

## Documentation

Find the detailed documentation [here](docs/README.md).

## Contribute

- Issue Tracker: https://gitlab.com/librecube/prototypes/micropython-cfdp/-/issues
- Source Code: https://gitlab.com/librecube/prototypes/micropython-cfdp

To learn more on how to successfully contribute please read the contributing
information in the [LibreCube guidelines](https://gitlab.com/librecube/guidelines).

## Support

If you are having issues, please let us know. Reach us at
[Matrix](https://app.element.io/#/room/#librecube.org:matrix.org)
or via [Email](mailto:info@librecube.org).

## License

The project is licensed under the MIT license. See the [LICENSE](./LICENSE.txt) file for details.
