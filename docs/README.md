# Documentation

## Differences to Python-CFDP

Large part of the code base was ported from the [Python CFDP implementation](https://gitlab.com/librecube/lib/python-cfdp) directly.
However, to accomodate for the particularities of the constrained microcontroller environment, a number of changes needed to be
applied. The files modified with respect to the Python CFDP implementation are:

- **cfdp/filestore/native.py**
- **cfdp/machines/base.py**
- **cfdp/machines/timer.py**
- **cfdp/transport/udp.py**
- **cfdp/transport/zmq.py**
- **cfdp/core.py**
- **cfdp/meta/message.py**

The changes to the files are as follows.

- **cfdp/filestore/native.py :**
  - `import`: `shutil` and `tempfile` modules haven't been implemented yet in micropython so  insted of them we looked for alternate ways to use their functionalities using `random` module for `tempfile` and for `shutil` we use additional code. `binascii` is used for operations related to byte to hex conversions.
  - `NativeFileStore.__init__`: Because `os` module is implemented in micropython for micropython os `os.path.abspath(rootpath)` isn't implemented and not really necessary. So we use the roothpath itself given while initializing the class.
  - `NativeFileStore.get_native_path`: Since os.path is not implemented yet only string addition is sufficient to virtual filestore path to absolute native path.
  - `NativeFileStore.get_virtual_path`: Here the code makes sure that if root path is present already in the path then it remove it.
  - `NativeFileStore.remove_directory`: Because `shutil` is not available (to delete a non-empty directory), we remove file by file first, then remove the directory.
  - `NativeFileStore.list_directory`: it uses the micropython implemention of `os.ilistdir` to list the directories. This function returns an iterator which then yields tuples corresponding to the entries in the directory that it is listing. With no argument it lists the current directory, otherwise it lists the directory given by dir. More about file operations can be learnt [here](https://docs.micropython.org/en/latest/library/uos.html#filesystem-access)
  - `NativeFileStore.is_file`: Since `os.path.isfile` is not implemented . Therfore `os.stat` gets the status of a file or directory. `0x8000` in `os.stat(filepath)[0]` represents a file and `0x4000` in `os.stat(filepath)[0]` represents a dir.
  - `NativeFileStore.get_size`: Since `os.path.getsize` is not implemented native file code is used to get file size.
  - `NativeFileStore.join_path`: Since `bytes.hex()` method is not supported in micropython we insted use `binascii.hexlify(a)` for required conversion.
  - `NativeTemporaryFileHandle.__init__`: Since tempfile is not yet implemented in micropython we use randomfile names and later delete it.

- **cfdp/machines/base.py**
  - `import`: `os` module is imported to perform `os.remove` operations.
  - `Machine._close_temp_file`: Delete the temp file by name when it closes since it is not bound to temp file properties.

- **cfdp/machines/timer.py**
  - `import`: Since threading is not yet implemented in micropython instead we use low level thread module.
  - `Timer.__init__`: `_thread.allocate_lock()` Initiates a new lock object. The lock is initially unlocked.
  - `Timer.restart`: Checks if the lock is already locked or not. If yes it releases the lock. If timeout is given then it acquires a lock for a given timeout duration. Then if a lock is not acquired it starts a new thread for `self.expired` function.
  - `Timer.cancel`: Checks if the lock is already locked if yes it releases the lock.

- **cfdp/transport/udp.py**
  - `import`: Since threading is not yet implemented in micropython instead we use low level thread module.
  - `DEFAULT_BUFFER_SIZE`: Due to memory constraints a small buffer is usually preferred.
  - `UdpTransport.__init__`: Instead of using a threading object kill method we simply use thread kill status variable to init thread status.
  - `UdpTransport.request`: Starts a new `thread` using `_thread` module
  - `UdpTransport.unbind`: Instead of using a threading object we simply use thread kill status variable to change thread status.
  - `UdpTransport._incoming_pdu_handler`: Removes current thread method as it is not really necessary and also as it is not supported in micropython. Adjusts buffer size considering memory constraints. Instead of using a threading object we simply use thread kill status variable to show thread status.
- **cfdp/transport/zmq.py**
  - depreciated as it is not supported in micropython.
- **cfdp/core.py**
  - `import`: Since threading is not yet implemented in micropython instead we use low level thread module.
  - `CfdpEntity.__init__`: Uses `_thread` method to start new event and message threads. Instead of using a threading object kill method we simply use thread kill status variable to init thread status.
  - `CfdpEntity._event_handler`: Uses event kill variable.
  - `CfdpEntity._message_handler`: Uses message kill variable.
  - `CfdpEntity.shutdown`: Uses message and event kill variable to change kill status.

- **cfdp/meta/message.py**
  - All instances of `int.from_bytes` have been changed according to support micropython. `from_bytes(bytes, byteorder)` parameters are used instead of `from_bytes(bytes, byteorder, *, signed=False)`
